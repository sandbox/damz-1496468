<?php

use Drupal\Core\Database\Query\Select;

class EntitySQLStorage /* implements EntityStorage */ {

  /**
   * Implements EntityStorageInterface::__construct().
   *
   * Sets basic variables.
   */
  public function __construct($entityType) {
    $this->entityType = $entityType;
    $this->entityInfo = entity_get_info($entityType);
    $this->entityCache = array();
    $this->hookLoadArguments = array();
    $this->idKey = $this->entityInfo['entity keys']['id'];

    // Check if the entity type supports revisions.
    if (!empty($this->entityInfo['entity keys']['revision'])) {
      $this->revisionKey = $this->entityInfo['entity keys']['revision'];
      $this->revisionTable = $this->entityInfo['revision table'];
    }
    else {
      $this->revisionKey = FALSE;
    }
  }

  /**
   * Implements EntityStorageInterface::load().
   */
  public function load($ids = array(), $conditions = array()) {
    // Revisions are not statically cached, and require a different query to
    // other conditions, so separate the revision id into its own variable.
    if ($this->revisionKey && isset($conditions[$this->revisionKey])) {
      $revision_id = $conditions[$this->revisionKey];
      unset($conditions[$this->revisionKey]);
    }
    else {
      $revision_id = FALSE;
    }

    // Build and execute the query.
    $query_result = $this->buildQuery($ids, $conditions, $revision_id)->execute();

    if (!empty($this->entityInfo['entity class'])) {
      // We provide the necessary arguments for PDO to create objects of the
      // specified entity class.
      // @see EntityInterface::__construct()
      $query_result->setFetchMode(PDO::FETCH_CLASS, $this->entityInfo['entity class'], array(array(), $this->entityType));
    }
    $entities = $query_result->fetchAllAssoc($this->idKey);

    // HACK: support taxonomy terms until http://drupal.org/node/1496612
    // is fixed.
    if ($this->entityType == 'taxonomy_term') {
      foreach ($entities as $entity) {
        $vocabulary = taxonomy_vocabulary_load($entity->vid);
        $entity->vocabulary_machine_name = $vocabulary->machine_name;
      }
    }

    // HACK: support comments until http://drupal.org/node/1496614 is fixed.
    if ($this->entityType == 'comment') {
      foreach ($entities as $entity) {
        $node = node_load($entity->nid);
        $entity->node_type = $node->type;
        if ($entity->uid) {
          $user = user_load($entity->uid);
          $entity->registered_name = $user->name;
        }
      }
    }

    // Load fields.
    if ($this->entityInfo['fieldable']) {
      $this->loadFields($entities, $revision_id ? FIELD_LOAD_REVISION : FIELD_LOAD_CURRENT);
    }
    return $entities;
  }

  protected function loadFields(&$entities, $age) {
    $field_info = field_info_field_by_ids();
    $load_current = $age == FIELD_LOAD_CURRENT;

    $fields = array();
    foreach ($entities as $entity) {
      list($id, $vid, $bundle) = entity_extract_ids($this->entityType, $entity);
      $instances = _field_invoke_get_instances($this->entityType, $bundle, array('deleted' => FALSE));

      foreach ($instances as $instance) {
        $field_name = $instance['field_name'];
        $field_id = $instance['field_id'];
        // Make sure all fields are present at least as empty arrays.
        if (!isset($queried_entities[$id]->{$field_name})) {
          $entities[$id]->{$field_name} = array();
        }
        $fields[$field_id][] = $id;
      }
    }

    foreach ($fields as $field_id => $ids) {
      $field = $field_info[$field_id];
      $field_name = $field['field_name'];
      $table = $load_current ? EntitySQLStorage::fieldTableName($field) : EntitySQLStorage::fieldRevisionTableName($field);

      $query = db_select($table, 't')
        ->fields('t')
        ->condition('entity_type', $this->entityType)
        ->condition($load_current ? 'entity_id' : 'revision_id', $ids, 'IN')
        ->condition('langcode', field_available_languages($this->entityType, $field), 'IN')
        ->orderBy('delta');

      if (empty($options['deleted'])) {
        $query->condition('deleted', 0);
      }

      $results = $query->execute();

      $delta_count = array();
      foreach ($results as $row) {
        if (!isset($delta_count[$row->entity_id][$row->langcode])) {
          $delta_count[$row->entity_id][$row->langcode] = 0;
        }

        if ($field['cardinality'] == FIELD_CARDINALITY_UNLIMITED || $delta_count[$row->entity_id][$row->langcode] < $field['cardinality']) {
          $item = array();
          // For each column declared by the field, populate the item
          // from the prefixed database column.
          foreach ($field['columns'] as $column => $attributes) {
            $column_name = EntitySQLStorage::fieldColumnName($field_name, $column);
            $item[$column] = $row->$column_name;
          }

          // Add the item to the field values for the entity.
          $entities[$row->entity_id]->{$field_name}[$row->langcode][] = $item;
          $delta_count[$row->entity_id][$row->langcode]++;
        }
      }
    }

    // Invoke field-type module's hook_field_load().
    $null = NULL;
    _field_invoke_multiple('load', $this->entityType, $entities, $age, $null);

    // Invoke hook_field_attach_load(): let other modules act on loading the
    // entity.
    // TODO: remove.
    module_invoke_all('field_attach_load', $this->entityType, $entities, $age);
  }

  /**
   * Builds the query to load the entity.
   *
   * This has full revision support. For entities requiring special queries,
   * the class can be extended, and the default query can be constructed by
   * calling parent::buildQuery(). This is usually necessary when the object
   * being loaded needs to be augmented with additional data from another
   * table, such as loading node type into comments or vocabulary machine name
   * into terms, however it can also support $conditions on different tables.
   * See CommentController::buildQuery() or TaxonomyTermController::buildQuery()
   * for examples.
   *
   * @param $ids
   *   An array of entity IDs, or FALSE to load all entities.
   * @param $conditions
   *   An array of conditions in the form 'field' => $value.
   * @param $revision_id
   *   The ID of the revision to load, or FALSE if this query is asking for the
   *   most current revision(s).
   *
   * @return SelectQuery
   *   A SelectQuery object for loading the entity.
   */
  protected function buildQuery($ids, $conditions = array(), $revision_id = FALSE) {
    $query = db_select($this->entityInfo['base table'], 'base');

    $query->addTag($this->entityType . '_load_multiple');

    if ($revision_id) {
      $query->join($this->revisionTable, 'revision', "revision.{$this->idKey} = base.{$this->idKey} AND revision.{$this->revisionKey} = :revisionId", array(':revisionId' => $revision_id));
    }
    elseif ($this->revisionKey) {
      $query->join($this->revisionTable, 'revision', "revision.{$this->revisionKey} = base.{$this->revisionKey}");
    }

    // Add fields from the {entity} table.
    $entity_fields = $this->entityInfo['schema_fields_sql']['base table'];

    if ($this->revisionKey) {
      // Add all fields from the {entity_revision} table.
      $entity_revision_fields = drupal_map_assoc($this->entityInfo['schema_fields_sql']['revision table']);
      // The id field is provided by entity, so remove it.
      unset($entity_revision_fields[$this->idKey]);

      // Prefix all fields from the revision table that are also fields by the
      // same name in the base table.
      $entity_field_keys = array_flip($entity_fields);
      foreach ($entity_revision_fields as $key => $revision_field) {
        if (isset($entity_field_keys[$revision_field])) {
          $revision_field = 'revision_' . $revision_field;
        }
        $query->addField('revision', $key, $revision_field);
      }
    }

    $query->fields('base', $entity_fields);

    if ($ids) {
      $query->condition("base.{$this->idKey}", $ids, 'IN');
    }
    if ($conditions) {
      foreach ($conditions as $field => $value) {
        $query->condition('base.' . $field, $value);
      }
    }
    return $query;
  }

  /**
   * Implements EntityStorageInterface::delete().
   */
  public function delete($ids) {
    $entities = $ids ? $this->load($ids) : FALSE;
    if (!$entities) {
      // If no IDs or invalid IDs were passed, do nothing.
      return;
    }
    $transaction = db_transaction();

    try {
      $ids = array_keys($entities);

      db_delete($this->entityInfo['base table'])
        ->condition($this->idKey, $ids, 'IN')
        ->execute();

      // TODO: purge revisions.

      // Purge fields.
      $this->deleteFields($entity);

      // Ignore slave server temporarily.
      db_ignore_slave();
    }
    catch (Exception $e) {
      $transaction->rollback();
      watchdog_exception($this->entityType, $e);
      throw new EntityStorageException($e->getMessage, $e->getCode, $e);
    }
  }

  public function deleteFields($entity) {
    list(,, $bundle) = entity_extract_ids($this->entityType, $entity);

    _field_invoke('delete', $this->entityType, $entity);

    foreach (field_info_instances($this->entityType, $bundle) as $instance) {
      $field = field_info_field_by_id($instance['field_id']);
      $this->fieldPurgeData($entity, $field, $instance);
    }

    // Let other modules act on deleting the entity.
    // TODO: Remove.
    module_invoke_all('field_attach_delete', $this->entityType, $entity);
  }

  /**
   * Implements EntityStorageInterface::deleteRevision().
   */
  public function deleteRevision($entity) {
    // TODO: delete data from the main table.

    $this->deleteRevisionFields($entity);
  }

  public function deleteRevisionFields($entity) {
    list($id, $vid, $bundle) = entity_extract_ids($this->entityType, $entity);

    _field_invoke('delete_revision', $this->entityType, $entity);

    if (isset($vid)) {
      $fields = array();
      foreach (field_info_instances($this->entityType, $bundle) as $instance) {
        $field = field_info_field_by_id($instance['field_id']);

        $revision_name = EntitySQLStorage::fieldRevisionTableName($field);
        db_delete($revision_name)
          ->condition('entity_type', $this->entityType)
          ->condition('entity_id', $id)
          ->condition('revision_id', $vid)
          ->execute();
      }
    }

    // Let other modules act on deleting the revision.
    // TODO: remove.
    module_invoke_all('field_attach_delete_revision', $this->entityType, $entity);
  }

  /**
   * Implements EntityStorageInterface::save().
   */
  public function save(EntityInterface $entity) {
    $transaction = db_transaction();
    try {
      if (!$entity->isNew()) {
        $return = drupal_write_record($this->entityInfo['base table'], $entity, $this->idKey);
        $this->saveFields($entity, FIELD_STORAGE_UPDATE);
      }
      else {
        $return = drupal_write_record($this->entityInfo['base table'], $entity);
        $this->saveFields($entity, FIELD_STORAGE_INSERT);
      }

      // Ignore slave server temporarily.
      db_ignore_slave();

      return $return;
    }
    catch (Exception $e) {
      $transaction->rollback();
      watchdog_exception($this->entityType, $e);
      throw new EntityStorageException($e->getMessage(), $e->getCode(), $e);
    }
  }

  public function saveFields($entity, $op) {
    _field_invoke_default($op, $this->entityType, $entity);
    _field_invoke($op, $this->entityType, $entity);

    list($id, $vid, $bundle) = entity_extract_ids($this->entityType, $entity);
    if (!isset($vid)) {
      $vid = $id;
    }

    $fields = array();
    foreach (field_info_instances($this->entityType, $bundle) as $instance) {
      $field = field_info_field_by_id($instance['field_id']);
      $field_id = $field['id'];
      $field_name = $field['field_name'];
      if (!empty($entity->$field_name)) {
        $fields[$field_id] = $field;
      }
    }

    foreach ($fields as $field_id => $field) {
      $field_name = $field['field_name'];
      $table_name = EntitySQLStorage::fieldTableName($field);
      $revision_name = EntitySQLStorage::fieldRevisionTableName($field);

      $all_langcodes = field_available_languages($this->entityType, $field);
      $field_langcodes = array_intersect($all_langcodes, array_keys((array) $entity->$field_name));

      // Delete and insert, rather than update, in case a value was added.
      if ($op == FIELD_STORAGE_UPDATE) {
        // Delete languages present in the incoming $entity->$field_name.
        // Delete all languages if $entity->$field_name is empty.
        $langcodes = !empty($entity->$field_name) ? $field_langcodes : $all_langcodes;
        if ($langcodes) {
          db_delete($table_name)
            ->condition('entity_type', $this->entityType)
            ->condition('entity_id', $id)
            ->condition('langcode', $langcodes, 'IN')
            ->execute();
          db_delete($revision_name)
            ->condition('entity_type', $this->entityType)
            ->condition('entity_id', $id)
            ->condition('revision_id', $vid)
            ->condition('langcode', $langcodes, 'IN')
            ->execute();
        }
      }

      // Prepare the multi-insert query.
      $do_insert = FALSE;
      $columns = array('entity_type', 'entity_id', 'revision_id', 'bundle', 'delta', 'langcode');
      foreach ($field['columns'] as $column => $attributes) {
        $columns[] = EntitySQLStorage::fieldColumnName($field_name, $column);
      }
      $query = db_insert($table_name)->fields($columns);
      $revision_query = db_insert($revision_name)->fields($columns);

      foreach ($field_langcodes as $langcode) {
        $items = (array) $entity->{$field_name}[$langcode];
        $delta_count = 0;
        foreach ($items as $delta => $item) {
          // We now know we have someting to insert.
          $do_insert = TRUE;
          $record = array(
            'entity_type' => $this->entityType,
            'entity_id' => $id,
            'revision_id' => $vid,
            'bundle' => $bundle,
            'delta' => $delta,
            'langcode' => $langcode,
          );
          foreach ($field['columns'] as $column => $attributes) {
            $record[EntitySQLStorage::fieldColumnName($field_name, $column)] = isset($item[$column]) ? $item[$column] : NULL;
          }
          $query->values($record);
          if (isset($vid)) {
            $revision_query->values($record);
          }

          if ($field['cardinality'] != FIELD_CARDINALITY_UNLIMITED && ++$delta_count == $field['cardinality']) {
            break;
          }
        }
      }

      // Execute the query if we have values to insert.
      if ($do_insert) {
        $query->execute();
        $revision_query->execute();
      }
    }

    // Let other modules act on updating the entity.
    // TODO: remove.
    module_invoke_all('field_attach_' . $op, $this->entityType, $entity);
  }

  /**
   * Generate a table name for a field data table.
   *
   * @param $field
   *   The field structure.
   * @return
   *   A string containing the generated name for the database table
   */
  public static function fieldTableName($field) {
    if ($field['deleted']) {
      return "field_deleted_data_{$field['id']}";
    }
    else {
      return "field_data_{$field['field_name']}";
    }
  }

  /**
   * Generate a table name for a field revision archive table.
   *
   * @param $name
   *   The field structure.
   * @return
   *   A string containing the generated name for the database table
   */
  public static function fieldRevisionTableName($field) {
    if ($field['deleted']) {
      return "field_deleted_revision_{$field['id']}";
    }
    else {
      return "field_revision_{$field['field_name']}";
    }
  }

  /**
   * Generate a column name for a field data table.
   *
   * @param $name
   *   The name of the field
   * @param $column
   *   The name of the column
   * @return
   *   A string containing a generated column name for a field data
   *   table that is unique among all other fields.
   */
  public static function fieldColumnName($name, $column) {
    return $name . '_' . $column;
  }

  /**
   * Generate an index name for a field data table.
   *
   * @param $name
   *   The name of the field
   * @param $column
   *   The name of the index
   * @return
   *   A string containing a generated index name for a field data
   *   table that is unique among all other fields.
   */
  protected static function fieldIndexName($name, $index) {
    return $name . '_' . $index;
  }

  /**
   * Return the database schema for a field. This may contain one or
   * more tables. Each table will contain the columns relevant for the
   * specified field. Leave the $field's 'columns' and 'indexes' keys
   * empty to get only the base schema.
   *
   * @param $field
   *   The field structure for which to generate a database schema.
   * @return
   *   One or more tables representing the schema for the field.
   */
  public static function fieldSchema($field) {
    $deleted = $field['deleted'] ? 'deleted ' : '';
    $current = array(
      'description' => "Data storage for {$deleted}field {$field['id']} ({$field['field_name']})",
      'fields' => array(
        'entity_type' => array(
          'type' => 'varchar',
          'length' => 128,
          'not null' => TRUE,
          'default' => '',
          'description' => 'The entity type this data is attached to',
        ),
        'bundle' => array(
          'type' => 'varchar',
          'length' => 128,
          'not null' => TRUE,
          'default' => '',
          'description' => 'The field instance bundle to which this row belongs, used when deleting a field instance',
        ),
        'deleted' => array(
          'type' => 'int',
          'size' => 'tiny',
          'not null' => TRUE,
          'default' => 0,
          'description' => 'A boolean indicating whether this data item has been deleted'
        ),
        'entity_id' => array(
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => TRUE,
          'description' => 'The entity id this data is attached to',
        ),
        'revision_id' => array(
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => FALSE,
          'description' => 'The entity revision id this data is attached to, or NULL if the entity type is not versioned',
        ),
        // @todo Consider storing language as integer.
        'langcode' => array(
          'type' => 'varchar',
          'length' => 32,
          'not null' => TRUE,
          'default' => '',
          'description' => 'The language code for this data item.',
        ),
        'delta' => array(
          'type' => 'int',
          'unsigned' => TRUE,
          'not null' => TRUE,
          'description' => 'The sequence number for this data item, used for multi-value fields',
        ),
      ),
      'primary key' => array('entity_type', 'entity_id', 'deleted', 'delta', 'langcode'),
      'indexes' => array(
        'entity_type' => array('entity_type'),
        'bundle' => array('bundle'),
        'deleted' => array('deleted'),
        'entity_id' => array('entity_id'),
        'revision_id' => array('revision_id'),
        'langcode' => array('langcode'),
      ),
    );

    $field += array('columns' => array(), 'indexes' => array(), 'foreign keys' => array());
    // Add field columns.
    foreach ($field['columns'] as $column_name => $attributes) {
      $real_name = EntitySQLStorage::fieldColumnName($field['field_name'], $column_name);
      $current['fields'][$real_name] = $attributes;
    }

    // Add indexes.
    foreach ($field['indexes'] as $index_name => $columns) {
      $real_name = EntitySQLStorage::fieldIndexName($field['field_name'], $index_name);
      foreach ($columns as $column_name) {
        $current['indexes'][$real_name][] = EntitySQLStorage::fieldColumnName($field['field_name'], $column_name);
      }
    }

    // Add foreign keys.
    foreach ($field['foreign keys'] as $specifier => $specification) {
      $real_name = EntitySQLStorage::fieldIndexName($field['field_name'], $specifier);
      $current['foreign keys'][$real_name]['table'] = $specification['table'];
      foreach ($specification['columns'] as $column => $referenced) {
        $sql_storage_column = EntitySQLStorage::fieldColumnName($field['field_name'], $column_name);
        $current['foreign keys'][$real_name]['columns'][$sql_storage_column] = $referenced;
      }
    }

    // Construct the revision table.
    $revision = $current;
    $revision['description'] = "Revision archive storage for {$deleted}field {$field['id']} ({$field['field_name']})";
    $revision['primary key'] = array('entity_type', 'entity_id', 'revision_id', 'deleted', 'delta', 'langcode');
    $revision['fields']['revision_id']['not null'] = TRUE;
    $revision['fields']['revision_id']['description'] = 'The entity revision id this data is attached to';

    return array(
      EntitySQLStorage::fieldTableName($field) => $current,
      EntitySQLStorage::fieldRevisionTableName($field) => $revision,
    );
  }

  /**
   * Implements EntityStorageFieldable::fieldInstanceCreate().
   */
  function fieldInstanceCreate($instance) {
    $field = field_info_field($instance['field_name']);
    $schema = EntitySQLStorage::fieldSchema($field);
    foreach ($schema as $name => $table) {
      if (!db_table_exists($name)) {
        db_create_table($name, $table);
      }
    }
    drupal_get_schema(NULL, TRUE);
  }

  /**
   * Implements EntityStorageFieldable::fieldInstanceDelete().
   */
  function fieldInstanceDelete($instance) {
    $field = field_info_field($instance['field_name']);
    $table_name = EntitySQLStorage::fieldTableName($field);
    $revision_name = EntitySQLStorage::fieldRevisionTableName($field);
    db_update($table_name)
      ->fields(array('deleted' => 1))
      ->condition('entity_type', $instance['entity_type'])
      ->condition('bundle', $instance['bundle'])
      ->execute();
    db_update($revision_name)
      ->fields(array('deleted' => 1))
      ->condition('entity_type', $instance['entity_type'])
      ->condition('bundle', $instance['bundle'])
      ->execute();
  }

  function fieldPurgeData($entity, $field, $instance) {
    list($id, $vid, $bundle) = entity_extract_ids($this->entityType, $entity);

    $table_name = EntitySQLStorage::fieldTableName($field);
    $revision_name = EntitySQLStorage::fieldRevisionTableName($field);
    db_delete($table_name)
      ->condition('entity_type', $this->entityType)
      ->condition('entity_id', $id)
      ->execute();
    db_delete($revision_name)
      ->condition('entity_type', $this->entityType)
      ->condition('entity_id', $id)
      ->execute();
  }

  /**
   * Mark a field for deletion.
   */
  function fieldDeleteInstance($field) {
    // Mark all data associated with the field for deletion.
    $field['deleted'] = 0;
    $table = EntitySQLStorage::fieldTableName($field);
    $revision_table = EntitySQLStorage::fieldRevisionTableName($field);
    db_update($table)
      ->fields(array('deleted' => 1))
      ->execute();

    // Move the table to a unique name while the table contents are being deleted.
    $field['deleted'] = 1;
    $new_table = EntitySQLStorage::fieldTableName($field);
    $revision_new_table = EntitySQLStorage::fieldRevisionTableName($field);
    db_rename_table($table, $new_table);
    db_rename_table($revision_table, $revision_new_table);
    drupal_get_schema(NULL, TRUE);
  }

  /**
   * Delete a field schema.
   */
  function fieldPurgeInstance($instance) {
    $field = field_info_field($instance['field_name']);
    $instances = field_read_instances(array('field_id' => $field['id']), array('include_deleted' => 1));

    if (count($instances) > 0) {
      // There are still instances of this field, nothing to do.
      return;
    }

    $table_name = EntitySQLStorage::fieldTableName($field);
    $revision_name = EntitySQLStorage::fieldRevisionTableName($field);
    db_drop_table($table_name);
    db_drop_table($revision_name);
  }

  function fieldUpdateForbid($field, $prior_field, $has_data) {
    if ($has_data && $field['columns'] != $prior_field['columns']) {
      throw new FieldUpdateForbiddenException("entity_sql_storage cannot change the schema for an existing field with data.");
    }
  }

  function fieldHasData($field) {
    // TODO: implement.
    return TRUE;
  }

  function updateField($field, $prior_field) {
    if (! $this->fieldHasData($field)) {
      // There is no data. Re-create the tables completely.

      if (Database::getConnection()->supportsTransactionalDDL()) {
        // If the database supports transactional DDL, we can go ahead and rely
        // on it. If not, we will have to rollback manually if something fails.
        $transaction = db_transaction();
      }

      try {
        $prior_schema = EntitySQLStorage::fieldSchema($prior_field);
        foreach ($prior_schema as $name => $table) {
          db_drop_table($name, $table);
        }
        $schema = EntitySQLStorage::fieldSchema($field);
        foreach ($schema as $name => $table) {
          db_create_table($name, $table);
        }
      }
      catch (Exception $e) {
        if (Database::getConnection()->supportsTransactionalDDL()) {
          $transaction->rollback();
        }
        else {
          // Recreate tables.
          $prior_schema = EntitySQLStorage::fieldSchema($prior_field);
          foreach ($prior_schema as $name => $table) {
            if (!db_table_exists($name)) {
              db_create_table($name, $table);
            }
          }
        }
        throw $e;
      }
    }
    else {
      // There is data, so there are no column changes. Drop all the
      // prior indexes and create all the new ones, except for all the
      // priors that exist unchanged.
      $table = EntitySQLStorage::fieldTableName($prior_field);
      $revision_table = EntitySQLStorage::fieldRevisionTableName($prior_field);
      foreach ($prior_field['indexes'] as $name => $columns) {
        if (!isset($field['indexes'][$name]) || $columns != $field['indexes'][$name]) {
          $real_name = EntitySQLStorage::fieldIndexName($field['field_name'], $name);
          db_drop_index($table, $real_name);
          db_drop_index($revision_table, $real_name);
        }
      }
      $table = EntitySQLStorage::fieldTableName($field);
      $revision_table = EntitySQLStorage::fieldRevisionTableName($field);
      foreach ($field['indexes'] as $name => $columns) {
        if (!isset($prior_field['indexes'][$name]) || $columns != $prior_field['indexes'][$name]) {
          $real_name = EntitySQLStorage::fieldIndexName($field['field_name'], $name);
          $real_columns = array();
          foreach ($columns as $column_name) {
            $real_columns[] = EntitySQLStorage::fieldColumnName($field['field_name'], $column_name);
          }
          db_add_index($table, $real_name, $real_columns);
          db_add_index($revision_table, $real_name, $real_columns);
        }
      }
    }
    drupal_get_schema(NULL, TRUE);
  }

  /**
   * Implements EntityStorageInterface::executeQuery().
   */
  public static function executeQuery(EntityFieldQuery $query) {
    // Start from the base table of the entity.
    $entity_type = $query->entityConditions['entity_type']['value'];
    unset($query->entityConditions['entity_type']);
    $entity_info = entity_get_info($entity_type);
    $select_query = db_select($entity_type, 'base');

    // Add fields.
    $select_query->addExpression(':entity_type', 'entity_type', array(':entity_type' => $entity_type));
    $select_query->addField('base', $entity_info['entity keys']['id'], 'entity_id');
    if (!empty($entity_info['entity keys']['revision'])) {
      $select_query->addField('base', $entity_info['entity keys']['revision'], 'revision_id');
    }
    else {
      $select_query->addField('base', $entity_info['entity keys']['id'], 'revision_id');
    }
    if (!empty($entity_info['entity keys']['bundle'])) {
      $select_query->addField('base', $entity_info['entity keys']['bundle'], 'bundle');
    }
    else {
      $select_query->addExpression(':bundle', 'bundle', array(':bundle' => $entity_type));
    }

    $id_key = $query->age == FIELD_LOAD_CURRENT ? 'entity_id' : 'revision_id';

    $groups = array();
    // Add field conditions.
    EntitySQLStorage::queryFieldConditions($query, $select_query, $query->fieldConditions, $entity_type, $entity_info, $groups, function ($field_name, $column) { return EntitySQLStorage::fieldColumnName($field_name, $column); });

    // Add field meta conditions.
    EntitySQLStorage::queryFieldConditions($query, $select_query, $query->fieldMetaConditions, $entity_type, $entity_info, $groups, function ($field_name, $column) { return $column; });

    // Is there a need to sort the query by property?
    $has_property_order = FALSE;
    foreach ($query->order as $order) {
      if ($order['type'] == 'property') {
        $has_property_order = TRUE;
      }
    }

    // Add property conditions.
    foreach ($query->propertyConditions as $property_condition) {
      $query->addCondition($select_query, "base." . $property_condition['column'], $property_condition);
    }
    // Add entity conditions.
    foreach ($query->entityConditions as $key => $condition) {
      if ($key == 'entity_id') {
        $key = 'id';
      }
      else if ($key == 'revision_id') {
        $key = 'revision';
      }
      $query->addCondition($select_query, "base." . $entity_info['entity keys'][$key], $condition);
    }

    // Order the query.
    $table_aliases = array();
    foreach ($query->order as $order) {
      if ($order['type'] == 'entity') {
        $key = $order['specifier'];
        $select_query->orderBy("base.$key", $order['direction']);
      }
      elseif ($order['type'] == 'field') {
        $specifier = $order['specifier'];
        $field = $specifier['field'];

        if (!isset($table_aliases[$field['field_name']])) {
          $tablename = $query->age == FIELD_LOAD_CURRENT ? EntitySQLStorage::fieldTableName($field) : EntitySQLStorage::fieldRevisionTableName($field);
          $table_aliases[$field['field_name']] = $select_query->join($tablename, NULL, "%alias.entity_type = :entity_type AND %alias.$id_key = base." . $entity_info['entity keys']['id'], array(':entity_type' => $entity_type));
        }

        $table_alias = $table_aliases[$field['field_name']];
        $sql_field = "$table_alias." . EntitySQLStorage::fieldColumnName($field['field_name'], $specifier['column']);
        $select_query->orderBy($sql_field, $order['direction']);
      }
      elseif ($order['type'] == 'property') {
        $select_query->orderBy("base." . $order['specifier'], $order['direction']);
      }
    }

    debug((string) $select_query);

    return $query->finishQuery($select_query, $id_key);
  }

  /**
   * Adds field (meta) conditions to the given query objects respecting groupings.
   *
   * @param EntityFieldQuery $query
   *   The field query object to be processed.
   * @param SelectQuery $select_query
   *   The SelectQuery that should get grouping conditions.
   * @param condtions
   *   The conditions to be added.
   * @param $table_aliases
   *   An associative array of table aliases keyed by field index.
   * @param $column_callback
   *   A callback that should return the column name to be used for the field
   *   conditions. Accepts a field name and a field column name as parameters.
   */
  protected static function queryFieldConditions(EntityFieldQuery $query, Select $select_query, $conditions, $entity_type, $entity_info, $groups, $column_callback) {
    foreach ($conditions as $key => $condition) {
      $field = $condition['field'];

      // Prepare the filter subquery if it hasn't been added yet.
      $key = (isset($condition['delta_group']) ? $condition['delta_group'] : "\0") . ':' . (isset($condition['language_group']) ? $condition['language_group'] : "\0");
      if (!isset($groups[$key])) {
        $filter_query = db_select($query->age == FIELD_LOAD_CURRENT ? EntitySQLStorage::fieldTableName($field) : EntitySQLStorage::fieldRevisionTableName($field), 't');
        $id_key = $query->age == FIELD_LOAD_CURRENT ? 'entity_id' : 'revision_id';
        $filter_query->addExpression('1');
        $filter_query->where('t.entity_type = :entity_type AND t.' . $id_key . ' = base.' . $entity_info['entity keys'][$query->age == FIELD_LOAD_CURRENT ? 'id' : 'revision'], array(':entity_type' => $entity_type));
        if (isset($query->deleted)) {
          $filter_query->condition("t.deleted", (int) $query->deleted);
        }
        $select_query->exists($filter_query);
        $groups[$key] = $filter_query;
      }
      else {
        $filter_query = $groups[$key];
      }

      // Add the specified condition.
      $sql_field = "t." . $column_callback($field['field_name'], $condition['column']);
      $query->addCondition($filter_query, $sql_field, $condition);
    }
  }

}
