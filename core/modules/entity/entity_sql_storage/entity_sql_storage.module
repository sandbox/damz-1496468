<?php

use Drupal\Core\Database\Database;
use Drupal\Core\Database\Query\Select;

/**
 * @file
 * Default implementation of the field storage API.
 */

/**
 * Implements hook_help().
 */
function entity_sql_storage_help($path, $arg) {
  switch ($path) {
    case 'admin/help#entity_sql_storage':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('The Entity SQL Storage module stores entity and field data in the database. It is the default entity storage module; other entity storage mechanisms may be available as contributed modules. See the <a href="@field-help">Field module help page</a> for more information about fields.', array('@field-help' => url('admin/help/field'))) . '</p>';
      return $output;
  }
}

/**
 * Implements hook_field_storage_info().
 */
function entity_sql_storage_field_storage_info() {
  return array(
    'entity_sql_storage' => array(
      'label' => t('Default SQL storage'),
      'description' => t('Stores fields in the local SQL database, using per-field tables.'),
    ),
  );
}

/**
 * Implements hook_field_storage_query().
 */
function entity_sql_storage_field_storage_query(EntityFieldQuery $query) {
  if ($query->age == FIELD_LOAD_CURRENT) {
    $tablename_function = '_entity_sql_storage_tablename';
    $id_key = 'entity_id';
  }
  else {
    $tablename_function = '_entity_sql_storage_revision_tablename';
    $id_key = 'revision_id';
  }
  $table_aliases = array();
  // Add tables for the fields used.
  foreach ($query->fields as $key => $field) {
    $tablename = $tablename_function($field);
    // Every field needs a new table.
    $table_alias = $tablename . $key;
    $table_aliases[$key] = $table_alias;
    if ($key) {
      $select_query->join($tablename, $table_alias, "$table_alias.entity_type = $field_base_table.entity_type AND $table_alias.$id_key = $field_base_table.$id_key");
    }
    else {
      $select_query = db_select($tablename, $table_alias);
      $select_query->addTag('entity_field_access');
      $select_query->addMetaData('base_table', $tablename);
      $select_query->fields($table_alias, array('entity_type', 'entity_id', 'revision_id', 'bundle'));
      $field_base_table = $table_alias;
    }
    if ($field['cardinality'] != 1 || $field['translatable']) {
      $select_query->distinct();
    }
  }

  // Add field conditions. We need a fresh grouping cache.
  drupal_static_reset('_entity_sql_storage_query_field_conditions');
  _entity_sql_storage_query_field_conditions($query, $select_query, $query->fieldConditions, $table_aliases, '_entity_sql_storage_columnname');

  // Add field meta conditions.
  _entity_sql_storage_query_field_conditions($query, $select_query, $query->fieldMetaConditions, $table_aliases, function ($field_name, $column) { return $column; });

  if (isset($query->deleted)) {
    $select_query->condition("$field_base_table.deleted", (int) $query->deleted);
  }

  // Is there a need to sort the query by property?
  $has_property_order = FALSE;
  foreach ($query->order as $order) {
    if ($order['type'] == 'property') {
      $has_property_order = TRUE;
    }
  }

  if ($query->propertyConditions || $has_property_order) {
    if (empty($query->entityConditions['entity_type']['value'])) {
      throw new EntityFieldQueryException('Property conditions and orders must have an entity type defined.');
    }
    $entity_type = $query->entityConditions['entity_type']['value'];
    $entity_base_table = _entity_sql_storage_query_join_entity($select_query, $entity_type, $field_base_table);
    $query->entityConditions['entity_type']['operator'] = '=';
    foreach ($query->propertyConditions as $property_condition) {
      $query->addCondition($select_query, "$entity_base_table." . $property_condition['column'], $property_condition);
    }
  }
  foreach ($query->entityConditions as $key => $condition) {
    $query->addCondition($select_query, "$field_base_table.$key", $condition);
  }

  // Order the query.
  foreach ($query->order as $order) {
    if ($order['type'] == 'entity') {
      $key = $order['specifier'];
      $select_query->orderBy("$field_base_table.$key", $order['direction']);
    }
    elseif ($order['type'] == 'field') {
      $specifier = $order['specifier'];
      $field = $specifier['field'];
      $table_alias = $table_aliases[$specifier['index']];
      $sql_field = "$table_alias." . _entity_sql_storage_columnname($field['field_name'], $specifier['column']);
      $select_query->orderBy($sql_field, $order['direction']);
    }
    elseif ($order['type'] == 'property') {
      $select_query->orderBy("$entity_base_table." . $order['specifier'], $order['direction']);
    }
  }

  return $query->finishQuery($select_query, $id_key);
}

/**
 * Adds the base entity table to a field query object.
 *
 * @param SelectQuery $select_query
 *   A SelectQuery containing at least one table as specified by
 *   _entity_sql_storage_tablename().
 * @param $entity_type
 *   The entity type for which the base table should be joined.
 * @param $field_base_table
 *   Name of a table in $select_query. As only INNER JOINs are used, it does
 *   not matter which.
 *
 * @return
 *   The name of the entity base table joined in.
 */
function _entity_sql_storage_query_join_entity(Select $select_query, $entity_type, $field_base_table) {
  $entity_info = entity_get_info($entity_type);
  $entity_base_table = $entity_info['base table'];
  $entity_field = $entity_info['entity keys']['id'];
  $select_query->join($entity_base_table, $entity_base_table, "$entity_base_table.$entity_field = $field_base_table.entity_id");
  return $entity_base_table;
}

/**
 * Adds field (meta) conditions to the given query objects respecting groupings.
 *
 * @param EntityFieldQuery $query
 *   The field query object to be processed.
 * @param SelectQuery $select_query
 *   The SelectQuery that should get grouping conditions.
 * @param condtions
 *   The conditions to be added.
 * @param $table_aliases
 *   An associative array of table aliases keyed by field index.
 * @param $column_callback
 *   A callback that should return the column name to be used for the field
 *   conditions. Accepts a field name and a field column name as parameters.
 */
function _entity_sql_storage_query_field_conditions(EntityFieldQuery $query, Select $select_query, $conditions, $table_aliases, $column_callback) {
  $groups = &drupal_static(__FUNCTION__, array());
  foreach ($conditions as $key => $condition) {
    $table_alias = $table_aliases[$key];
    $field = $condition['field'];
    // Add the specified condition.
    $sql_field = "$table_alias." . $column_callback($field['field_name'], $condition['column']);
    $query->addCondition($select_query, $sql_field, $condition);
    // Add delta / langcode group conditions.
    foreach (array('delta', 'langcode') as $column) {
      if (isset($condition[$column . '_group'])) {
        $group_name = $condition[$column . '_group'];
        if (!isset($groups[$column][$group_name])) {
          $groups[$column][$group_name] = $table_alias;
        }
        else {
          $select_query->where("$table_alias.$column = " . $groups[$column][$group_name] . ".$column");
        }
      }
    }
  }
}

/**
 * Implements hook_field_attach_rename_bundle().
 */
function entity_sql_storage_field_attach_rename_bundle($entity_type, $bundle_old, $bundle_new) {
  $entity_info = entity_get_info($entity_type);
  if ($entity_info['storage controller class'] != 'EntitySQLStorage') {
    return;
  }

  // We need to account for deleted or inactive fields and instances.
  $instances = field_read_instances(array('entity_type' => $entity_type, 'bundle' => $bundle_new), array('include_deleted' => TRUE, 'include_inactive' => TRUE));
  foreach ($instances as $instance) {
    $field = field_info_field_by_id($instance['field_id']);

    $table_name = EntitySQLStorage::fieldTableName($field);
    $revision_name = EntitySQLStorage::fieldTableName($field);
    db_update($table_name)
      ->fields(array('bundle' => $bundle_new))
      ->condition('entity_type', $entity_type)
      ->condition('bundle', $bundle_old)
      ->execute();
    db_update($revision_name)
      ->fields(array('bundle' => $bundle_new))
      ->condition('entity_type', $entity_type)
      ->condition('bundle', $bundle_old)
      ->execute();
  }
}

/**
 * Implements hook_field_storage_details().
 */
function entity_sql_storage_field_storage_details($field) {
  $details = array();
  if (!empty($field['columns'])) {
     // Add field columns.
    foreach ($field['columns'] as $column_name => $attributes) {
      $real_name = _entity_sql_storage_columnname($field['field_name'], $column_name);
      $columns[$column_name] = $real_name;
    }
    return array(
      'sql' => array(
        FIELD_LOAD_CURRENT => array(
          _entity_sql_storage_tablename($field) => $columns,
        ),
        FIELD_LOAD_REVISION => array(
          _entity_sql_storage_revision_tablename($field) => $columns,
        ),
      ),
    );
  }
}
